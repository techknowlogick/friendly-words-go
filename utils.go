package words

import (
	"fmt"
	"math/rand"
)

var (
	sampleSize = 10
)

func pairs(sliceOne, sliceTwo []string) []string {
	var pairs []string
	if len(sliceOne) != len(sliceTwo) {
		return pairs
	}

	for _, one := range sliceOne {
		for _, two := range sliceTwo {
			pairs = append(pairs, fmt.Sprintf("%s-%s", one, two))
		}
	}

	return pairs
}

// Fisher-Yates shuffle algorithm
func shuffle(slice []string) []string {
	shuffled := make([]string, len(slice))
	copy(shuffled, slice)

	for i := len(shuffled) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		shuffled[i], shuffled[j] = shuffled[j], shuffled[i]
	}

	return shuffled
}

func randomSample(slice []string, n int) []string {
	if n > len(slice) {
		n = len(slice)
	}

	shuffled := shuffle(slice)
	return shuffled[:n]
}
