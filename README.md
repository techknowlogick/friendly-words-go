# Friendly Words - Go


## Credits

The words in this repository are from Glitch.com's [Friendly Words](https://github.com/glitchdotcom/friendly-words) repository, and the logic is based upon their javascript implementation. Their code (and words?) is licensed under the MIT license.