package words

func WordPairs() []string {
	return WordPairsWithSize(sampleSize)
}

func WordPairsWithSize(size int) []string {
	return pairs(randomSample(Predicates, size), randomSample(Objects, size))
}
