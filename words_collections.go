package words

// This is a list of synonyms for "collection".

func CollectionSamples() []string {
	return WordPairsWithSize(sampleSize)
}

func CollectionSamplesWithSize(size int) []string {
	return randomSample(Collections, size)
}

func CollectionPairs() []string {
	return CollectionPairsWithSize(sampleSize)
}

func CollectionPairsWithSize(size int) []string {
	return pairs(randomSample(Predicates, size), randomSample(Collections, size))
}

var (
	Collections = []string{
		"accrual",
		"aggregation",
		"album",
		"analects",
		"anthology",
		"array",
		"assembly",
		"assortment",
		"bank",
		"batch",
		"bouquet",
		"bricolage",
		"bushel",
		"clump",
		"clutch",
		"collection",
		"collective",
		"compilation",
		"conglomeration",
		"constellation",
		"contribution",
		"cumulation",
		"digest",
		"everything",
		"flight",
		"florilegium",
		"garland",
		"gathering",
		"group",
		"gumbo",
		"heap",
		"herd",
		"hoard",
		"jumble",
		"kaleidoscope",
		"kit",
		"list",
		"lot",
		"mashup",
		"medley",
		"melange",
		"menu",
		"mine",
		"miscellany",
		"mishmash",
		"mix",
		"nest",
		"organization",
		"pack",
		"packet",
		"parcel",
		"playlist",
		"pool",
		"potpourri",
		"range",
		"reserve",
		"reservoir",
		"roll",
		"selection",
		"set",
		"shrewdness",
		"spicilege",
		"stack",
		"stockpile",
		"store",
		"supply",
		"treasure",
		"treasury",
		"troupe",
		"variety",
	}

	collectionsLen = len(Collections)
)
